package com.dake.jdk8.test;

import com.dake.jdk8.reflect.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanCopier;

import java.time.LocalDate;


public class BeanCopyTest {

    public static void main(String[] args) {
        testDifferentTypeWithSameNameOfParam1();
        testDifferentTypeWithSameNameOfParam2();
    }

    public static void testDifferentTypeWithSameNameOfParam1(){
        A a = new A("2021-02");
        B b = new B();
        BeanUtils.copyProperties(a, b);
        System.out.println(b.getDate());

    }

    public static void testDifferentTypeWithSameNameOfParam2(){
        A a = new A("2021-02");
        B b = new B();
        BeanCopier beanCopier = BeanCopier.create(A.class, B.class, false);
        beanCopier.copy(a, b, null);
        System.out.println(b.getDate());

    }
}

class A {
    public String date;

    public A(String date) {
        this.date = date;
    }
}

class B {

    public LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public B(LocalDate date) {
        this.date = date;
    }

    public B() {
    }
}