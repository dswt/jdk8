package com.dake.jdk8.reflect;

import com.dake.jdk8.util.ReflectionUtils;
import com.dake.jdk8.vo.reflect.SingleObject;
import com.dake.jdk8.vo.reflect.Student;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class Reflect_08 {

    public static void main(String[] args) throws NoSuchFieldException {
        Student student = new Student();
//        student.setAge("18");
//        student.setId("1");
//        student.setName("zhangsan");
//        student.setScore("80");

        SingleObject singleObject = new SingleObject();

        String b = "1";
        System.out.println(b.getClass().getTypeName());
        System.out.println(b.getClass().getName());
        System.out.println(singleObject.getClass().getTypeName());
        System.out.println(singleObject.getClass().getName());
        System.out.println("---------------------------------1111111111-----------------------------");
        System.out.println(singleObject.getClass().getDeclaredField("student").getName());
        System.out.println(singleObject.getClass().getDeclaredField("student").getType() == Student.class);
        System.out.println(singleObject.getClass().getDeclaredField("studentList").getGenericType().getTypeName());

        Object c = true;
        System.out.println(c.getClass().getTypeName());
        System.out.println(c.getClass().getName());

        List<String> d = Arrays.asList("12", "b");
        String[] e = {"b", "er"};
        System.out.println(e.getClass().getName());
        System.out.println(e.getClass().getTypeName());

        System.out.println(d.getClass().getName());
        System.out.println(d.getClass().getTypeName());

//        d.add("f");
//        d.set(2, "t");
        d.forEach(System.out::println);

        Long f = 100L;

//        ReflectionUtils.setValue2Object(singleObject, "id", "01");
//        singleObject.setStudent(student);

        if (f.getClass().getTypeName().equals("java.lang.Long")) {
        }

        System.out.println(Long.class.getGenericSuperclass());
//        System.out.println(("class java.lang.Long").equals(id.getGenericType().toString()));
        System.out.println("----------------------------------------------------------------------");

        Field length = singleObject.getClass().getDeclaredField("length");
        Field idd = singleObject.getClass().getDeclaredField("idd");
        Field score = singleObject.getClass().getDeclaredField("score");
        Field id = singleObject.getClass().getDeclaredField("id");
        Field gender = singleObject.getClass().getDeclaredField("gender");
        Field heigh = singleObject.getClass().getDeclaredField("heigh");
        Field height = singleObject.getClass().getDeclaredField("height");
        Field name = singleObject.getClass().getDeclaredField("name");
        Field student1 = singleObject.getClass().getDeclaredField("student");
        Field stringList = singleObject.getClass().getDeclaredField("stringList");
        Field studentList = singleObject.getClass().getDeclaredField("studentList");

        System.out.println("Byte对应的取值：" + (length.getType().equals(Byte.class)));
        System.out.println("Byte对应的取值：" + length.getType().getSimpleName());
        System.out.println("Byte对应的取值：" + length.getGenericType().toString());
        System.out.println("Short对应的取值：" + idd.getGenericType().toString());
        System.out.println("Integer对应的取值：" + score.getGenericType().toString());
        System.out.println("Long对应的取值：" + id.getGenericType().toString());
        System.out.println("Boolean对应的取值：" + gender.getGenericType().toString());
        System.out.println("Float对应的取值：" + heigh.getGenericType().toString());
        System.out.println("Double对应的取值：" + height.getGenericType().toString());
        System.out.println("String对应的取值：" + name.getGenericType().toString());
        System.out.println("对象对应的取值：" + student1.getGenericType().toString());
        System.out.println("对象对应的取值：" + student1.getType().getSimpleName());
        System.out.println("对象对应的取值：" + (student1.getType() == Student.class));
        System.out.println("对象对应的取值：" + (stringList.getType() == List.class));
        System.out.println("对象对应的取值：" + (studentList.getType() == List.class));

    }

    public static <T> T assign2Object(T t, String field, String value){


        ReflectionUtils.setValue2Object(t, field, value);
        return t;
    }
}
