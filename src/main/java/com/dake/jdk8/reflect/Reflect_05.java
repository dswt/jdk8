package com.dake.jdk8.reflect;

import com.dake.jdk8.util.ReflectionUtils;
import com.dake.jdk8.vo.reflect.Student;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Reflect_05 {

    public static void main(String[] args) {
        Student zhangsan = new Student("1", "zhangsan", "100", "18");
        LinkedHashMap<String, Object> stringObjectMap = (LinkedHashMap<String, Object>) ReflectionUtils.object2Map(zhangsan);
        Iterator<Map.Entry<String, Object>> iterator = stringObjectMap.entrySet().stream().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            System.out.println(next.getKey());
        }

    }
}
