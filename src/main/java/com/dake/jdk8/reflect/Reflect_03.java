package com.dake.jdk8.reflect;

import java.lang.reflect.Constructor;

public class Reflect_03 {
    public static void main(String[] args) {
        String menuAction = "gef.putin.step.ui.SystemMenuTest(xiajiaji)";
        boolean endsWith = menuAction.endsWith(")");//判断是否带参数
        SystemMenuTest action = null;
        if (!endsWith) {
            try {
                action = (SystemMenuTest) Class.forName(menuAction).newInstance();
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        } else {
            String clazzName = menuAction.substring(0, menuAction.indexOf("("));
            String parAllStr = menuAction.substring(menuAction.indexOf("(") + 1, menuAction.indexOf(")"));//取参数名
            boolean contains = parAllStr.contains(",");
            String[] parStr = null;
            if (contains) {
                parStr = parAllStr.split(",");
            } else {
                parStr = new String[]{parAllStr};
            }
            try {
                Class clazz = Class.forName(clazzName);
                Class[] parClass = new Class[parStr.length];
                for (int i = 0; i < parStr.length; i++) {
                    parClass[i] = String.class;
                }
                Constructor con = clazz.getConstructor(parClass);
                action = (SystemMenuTest) con.newInstance(parStr);
            } catch (Exception exe) {
                exe.printStackTrace();
            }
        }
        action.show();
    }
}

class SystemMenuTest {
    private String name;

    public SystemMenuTest(String name) {
        this.name = name;
    }

    public void show() {
        System.out.println("要转换类中的参数是：" + name);
    }
}
