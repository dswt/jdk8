package com.dake.jdk8.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Reflect_04 {

    private static final String FULL_LIMIT_NAME = "com.dake.jdk8.vo.reflect";
    private static final String SEPARATOR = ".";

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException,
            NoSuchFieldException, InvocationTargetException, ClassNotFoundException {
        String str = "Person.Student";
        String value = "18";
        transStr2Object(str, value);
    }

    public static void transStr2Object(String str, String value) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException, NoSuchFieldException {
        String[] split = str.split(SEPARATOR);
        System.out.println(split.length);
        for (int i = split.length - 1; i > 0; i--) {
            String s = split[i];
            Class<?> aClass = Class.forName(FULL_LIMIT_NAME + SEPARATOR + s);
//            aClass.newInstance();
            Object o = aClass.getDeclaredConstructor().newInstance();
            Field age = o.getClass().getDeclaredField("age");
            age.setAccessible(true);
            age.set(o, value);

            String s1 = split[i - 1];
            Class<?> aClass1 = Class.forName(FULL_LIMIT_NAME + SEPARATOR + s1);
            Object o1 = aClass1.getDeclaredConstructor().newInstance();

            Field declaredField = o1.getClass().getDeclaredField(s);
            declaredField.setAccessible(true);
            declaredField.set(o1, s);

            System.out.println(o);
            System.out.println(o1);
        }

    }

}
