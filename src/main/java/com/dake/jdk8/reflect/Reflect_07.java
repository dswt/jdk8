package com.dake.jdk8.reflect;

import com.dake.jdk8.vo.reflect.A;
import com.dake.jdk8.vo.reflect.Bef;
import com.dake.jdk8.vo.reflect.Student;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.locks.ReadWriteLock;

public class Reflect_07 {

    public static void main(String[] args) {
        /*String simpleName = Bef.class.getSimpleName();
        String s = A.class.toString();
        System.out.println(simpleName);
        System.out.println(s);*/

        testMethod();
    }

    private Object assign2QueryCalss(Object object, String paramName, Object paramValue) {
        try {
            Method declaredMethod = object.getClass().getDeclaredMethod("and" + paramName.substring(0, 1).toUpperCase()
                    + paramName.substring(1) + "EqualTo", object.getClass());
            return declaredMethod.invoke(object, paramName);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("反射方法异常", e);
        }
    }

    private static void testMethod(){
        Student student = new Student();
//        student.setAge("18");
        try {
            Method method = Student.class.getDeclaredMethod("getAge");
            method.setAccessible(true);
            Object invoke = method.invoke(student);
            System.out.println(invoke);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private <T extends Object> T test(){


        return null;

    }
}
