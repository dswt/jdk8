package com.dake.jdk8.strings;

import com.dake.jdk8.reflect.Refkect_06;
import com.dake.jdk8.vo.reflect.Bef;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class String_01 {



    public static void main(String[] args) {

        System.out.println(".abc.".indexOf("."));

        System.out.println("abc.ccc".indexOf("."));
        System.out.println("abc.ccc1".indexOf("1"));
        System.out.println("abc".lastIndexOf("-"));
        System.out.println("abc".substring("abc".lastIndexOf("]") + 1));

        System.out.println("------------------aaaa----------------------------");
        System.out.println("abc.def[0].aaa".lastIndexOf("."));
        String[] arr = "abc.def[0].aaa".split("\\.");
        System.out.println(arr.length);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        System.out.println("-------------------测试_分隔符--------------------");
        System.out.println("abc_def[0]_aaa".split("_").length);

        System.out.println("-------------------测试中文逗号分隔符--------------------");
        String[] arr1 = "abc，def[0]，aaa".split("\\]");
        System.out.println(arr1.length);
        for (int i = 0; i < arr1.length; i++) {
            System.out.println(arr1[i]);
        }

        System.out.println("---------------集合分组----------------");
        Refkect_06.generateList();

        List<Bef> list = new ArrayList<>();
        Bef bef = new Bef();
        bef.setAge("18");
        bef.setName("zhangsan");
        Bef bef1 = new Bef();
        bef1.setAge("28");
        bef1.setName("zhangsan1");
//        list.set(1, bef);
        list.add(0, bef1);
        list.add(1, bef);
        list.forEach(System.out::println);


/*        Map<String, List<String>> collect = asList.stream().collect(Collectors.groupingBy(str -> {
            if (!str.contains("[")) {
                return str.substring(0, str.indexOf("."));
            } else {
                String[] split = str.split("\\.");
                return split[0] + "." + split[1];
//                return str.substring(0, str.indexOf("]") + 1);
            }
        }, Collectors.toList()));

        collect.forEach((k, v) -> {
            System.out.println("k = " + k + ", v = " + v);
        });*/
        /*
            s -> 上一层对象
         */

        StringJoiner stringJoiner = new StringJoiner(".");
        stringJoiner.add("abc").add("bn");
        System.out.println(stringJoiner);
        System.out.println("--------------------------test contentEquals-----------------");

        List<String> list1 = Arrays.asList("an", "bn", "cd");
        String[] s1 = {"an", "bn", "cd"};
        String join = String.join(".", s1);
        String collect = Stream.of(s1).collect(Collectors.joining("."));
        System.out.println("join = " + join);
        System.out.println(collect);
        System.out.println("c".contentEquals("c"));

        "abc.def".split("\\.");

        System.out.println("ab.".substring(0, "ab.".length()));

    }

    @Transactional(isolation = Isolation.REPEATABLE_READ, readOnly = false, propagation = Propagation.REQUIRED)
    public String testTransaction() {
        return null;
    }

}
