package com.dake.jdk8.strings;

import java.text.DecimalFormat;

public class String_02 {

    public static void main(String[] args) {
        String[] strings = {"a"};
        String a = null +"a" + null;
        System.out.println(a);

    }

    public static String supplement0FrontStrInsufficientLength(int length, int number) {
        String formatter = "%0" + length + "d";
//        return String.format(formatter, number);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append("0");
        }

        return new DecimalFormat(sb.toString()).format(number);



    }


}
