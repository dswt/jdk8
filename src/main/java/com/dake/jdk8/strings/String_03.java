package com.dake.jdk8.strings;

import org.springframework.util.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class String_03 {
    public static void main(String[] args) {
        TestStringHash testStringHash = new TestStringHash();
        System.out.println(testStringHash.hashCode());
        System.out.println("---------------0x------------");
        System.out.println(0xff);
        System.out.println(0X10FFFF);
        int index = 1;
//        index <<= 1;
        index = index << 1;
        System.out.println(index);

        System.out.println("--------------------test length（）----------------");
        String str = "中国";
        int length = str.length();
        int hashCode = str.hashCode();
        System.out.println(hashCode);
        System.out.println(length);

        System.out.println("-------------------");
        convertDecimal2Binary(255);

        System.out.println("-------------字符编码-------------------");
        String s = Charset.defaultCharset().displayName();
        System.out.println(s);
        String bt = "中国";
        byte[] bytes1 = bt.getBytes();
        byte[] bytes2 = bt.getBytes(StandardCharsets.UTF_8);
        byte[] bytes3 = bt.getBytes(StandardCharsets.UTF_16);
        byte[] bytes4 = bt.getBytes(StandardCharsets.ISO_8859_1);
        System.out.println(bytes1.length);
        System.out.println(bytes2.length);
        System.out.println(bytes3.length);
        System.out.println(bytes4.length);
    }

   public static void convertDecimal2Binary(int decimalNumber) {
       System.out.println(Integer.toBinaryString(decimalNumber));
       System.out.println("&&&&&&&&测试&&&&&");
       System.out.println(7 & 3);
       int index = 3;
       index >>= 1;
       System.out.println(index);

       System.out.println("|||||||||||||测试|||||||||||||||||");
       System.out.println(5 | 3);
   }

    public static <T>  void testInstance(T t){
        String a = "a";
        String b = "b";

        if (t instanceof Integer) {
            System.out.println((Integer)t == 1);
        }
    }
}
