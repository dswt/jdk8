package com.dake.jdk8.strings;

/**
 * 字节预算
 * {@see StringUTF16}类（因为该类是默认的类修饰符，只能在lang包下面被访问，所以无法直接饮用）中的变量命名：
 * <blockquote>
 *     <pre>
 *     static final int HI_BYTE_SHIFT;
 *     static final int LO_BYTE_SHIFT;
 *     static {
 *         if (isBigEndian()) {
 *             HI_BYTE_SHIFT = 8;
 *             LO_BYTE_SHIFT = 0;
 *         } else {
 *             HI_BYTE_SHIFT = 0;
 *             LO_BYTE_SHIFT = 8;
 *         }
 *     }
 *     </pre>
 * </blockquote>
 * 分别是指高字节移位与低字节移位。其中方法{@code isBigEndian}是本地方法（native），意思是指是大端字节序，相应的还有小端字节序（little endian）。
 *
 * @date 2021-2-5 16:54:37
 * @author fangqi
 */
public class ByteOpetation {

    public static void main(String[] args) {
        System.out.println("与操作——&:" + (3 & 5));
        logicalOperation();
    }

    public void byteShift(){
        System.out.println(3 << 1);
        System.out.println(3 >> 1);
        System.out.println(-3 >> 1);
        System.out.println(3 >>> 1);
        System.out.println(-3 >>> 1);
    }

    public static void logicalOperation() {
        // 逻辑与（&），都为1才为1，否则为0
        System.out.println("与操作（&）:" + (3 & 5));
        // 逻辑或（|），只要有一个为1就为1，否则为0
        System.out.println("或操作（|）:" + (6 | 5));
        System.out.println("与操作（&）:" + (3 & 5));
        System.out.println("与操作（&）:" + (3 & 5));
    }

}
