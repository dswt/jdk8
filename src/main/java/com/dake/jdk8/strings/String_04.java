package com.dake.jdk8.strings;

import org.apache.logging.log4j.util.Strings;

import java.lang.reflect.Array;

class String_04 {

    public static void main(String[] args) {
        String[] strings = {"a", "b"};
        System.out.println(int.class);
        System.out.println(strings.getClass().isArray());
        testDefault();
        System.out.println("-----------------------");
        String b = null;
        String a = "abc" + b ;
        System.out.println(a);
    }

    public static void testDefault() {
        String[] strings = {"a", "b"};
        if (strings.getClass().isInstance(Array.class)) {
            System.out.println(1);
        }
    }

}
