package com.dake.jdk8.strings;

public class TestStringHash {
    private int hash;


    public static void main(String[] args) {
        String_04.testDefault();
    }

    public void testHash(TestStringHash testStringHash) {
        this.hash = testStringHash.hash;
    }

    public int hashCode (){
        int h = hash;
        return h;
    }


    public static void testHashCode(){
        String a = "abc";
        String b = "cba";
        System.out.println(a.hashCode() == b.hashCode());// false
        String c = "gdejicbegh";
        String d = "hgebcijedg";
        System.out.println(c.hashCode() == d.hashCode());// true

        String e = "abcdefg";
        String f = "gfedcba";
        System.out.println(e.hashCode() == f.hashCode());// false


    }
}
