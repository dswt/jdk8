package com.dake.jdk8.vo.reflect;

import java.util.List;

public class SingleObject {

    private List<String> stringList;
    private List<Student> studentList;
    private Student student;
    private Byte length;
    private Short idd;
    private Integer score;
    private Long id;
    private Boolean gender;
    private Float heigh;
    private Double height;
    private String name;


    public SingleObject(Student student, long id) {
        this.student = student;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public SingleObject() {
    }
}
