package com.dake.jdk8.vo.reflect;

import java.util.List;

public class A {

    private String name;
    private String age;
    private List<Aef> aefList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<Aef> getAefList() {
        return aefList;
    }

    public void setAefList(List<Aef> aefList) {
        this.aefList = aefList;
    }
}
