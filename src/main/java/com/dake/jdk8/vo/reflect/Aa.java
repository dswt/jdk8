package com.dake.jdk8.vo.reflect;

import java.util.List;

public class Aa {

    public Aa() {
    }

    private String aParam1;
    private List<Bb> bbList;

    public List<Bb> getBbList() {
        return bbList;
    }

    public void setBbList(List<Bb> bbList) {
        this.bbList = bbList;
    }

    public String getaParam1() {
        return aParam1;
    }

    public void setaParam1(String aParam1) {
        this.aParam1 = aParam1;
    }

}
