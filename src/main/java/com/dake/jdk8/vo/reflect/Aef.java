package com.dake.jdk8.vo.reflect;

import java.util.List;

public class Aef {
    private String name;
    private String age;
    private List<Fgh> fghList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<Fgh> getFghList() {
        return fghList;
    }

    public void setFghList(List<Fgh> fghList) {
        this.fghList = fghList;
    }
}
