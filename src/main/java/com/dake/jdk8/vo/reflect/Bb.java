package com.dake.jdk8.vo.reflect;

import java.util.List;

public class Bb {

    public Bb() {
    }

    private String name;

    private List<Cc> ccList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Cc> getCcList() {
        return ccList;
    }

    public void setCcList(List<Cc> ccList) {
        this.ccList = ccList;
    }
}
