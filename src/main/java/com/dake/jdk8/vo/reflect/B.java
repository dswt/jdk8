package com.dake.jdk8.vo.reflect;

import java.util.List;

public class B {

    private String name;
    private String age;
    private List<Bef> befList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<Bef> getBefList() {
        return befList;
    }

    public void setBefList(List<Bef> befList) {
        this.befList = befList;
    }
}
