package com.dake.jdk8.vo;

public class Man extends Human{

    @Override
    public void say() {
        System.out.println("男人说，征服了天下就征服了女人");
    }

    @Override
    public Food eat() {
        return null;
    }
}
