package com.dake.jdk8.vo;

/**
 * @author Administrator
 */
public interface TestT<T> {

    /**
     * 测试泛型的接口
     *
     * @param t 入参
     * @return
     */
    T getData(T t);
}

class TestT2<T> {

    public T test(T a) {
        System.out.println(a);
        return a;
    }

    // 编译不通过
  /*  public T test1(String a) {
        System.out.println(a);
        return a;
    }*/

    // 编译不通过
   /* public <T> T test2(String a) {
        System.out.println(a);
        return a;
    }*/
}

class TestT3 {

    public static void main(String[] args) {
        new TestT2<>().test("a");
        test("t");
        test1("1");
        test2("2");
    }

    public static <T> T test(String a) {
        System.out.println(a);
        return (T) a;
    }

    public static <T> T test1(String a) {
        System.out.println(a);
        return (T) a;
    }

    public static <T> T test2(T a) {
        System.out.println(a);
        return a;
    }
}