package com.dake.jdk8.vo.impl;

import com.dake.jdk8.vo.TestT;

/**
 * @date 2021-1-7 22:13:39
 * @author fangqi
 */
public class SubTestT implements TestT {

    @Override
    public Object getData(Object o) {
        if (o.getClass() == String.class) {
            return o;
        }

        return null;
    }
}
