package com.dake.jdk8.conllection.map;

import com.dake.jdk8.vo.TestSingleObjectTwoInstance;
import com.dake.jdk8.vo.User;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Map_01 {
    private static final Map<String, String> MAP = new HashMap<>();

    static {
        MAP.put("1", "abc");
        MAP.put("2", "bca");
    }

    public static void main(String[] args) {
        System.out.println(MAP.get("1"));
        System.out.println(LocalDate.parse("2020-01-06"));

        User user = new User();
        user.setAge(10);
        user.setId(1);
        TestSingleObjectTwoInstance testSingleObjectTwoInstance = new TestSingleObjectTwoInstance();
        testSingleObjectTwoInstance.setUser1(user);
        testSingleObjectTwoInstance.setUser2(user);

        System.out.println(testSingleObjectTwoInstance.getUser1().getAge());
        System.out.println(testSingleObjectTwoInstance.getUser2().getId());

//        System.out.println(LocalDate.parse("2020年01月18日"));

        Map<String, Map<String, String>> map = new HashMap<>();
        System.out.println(map.get("1"));

        Map<String, String> map1 = new HashMap<>();
        map1.put("a", "b");
        Set<Map.Entry<String, String>> entries = map1.entrySet();
        Iterator<Map.Entry<String, String>> iterator = entries.iterator();

        Set<String> strings = map1.keySet();
        String s = strings.stream().findAny().get();
        System.out.println(s.equals("a"));

    }


}
