package com.dake.jdk8.conllection.list;

import com.dake.jdk8.vo.reflect.Person;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class List_01 {

    private static final List<Integer> INTEGER_LIST = new ArrayList<>();

    static {
        INTEGER_LIST.add(3);
        INTEGER_LIST.add(2);
        INTEGER_LIST.add(5);
    }

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        Person zhangsan = new Person("zhangsan", 30);
        Person lisi = new Person("lisi", 20);
        Person wangwu = new Person("wangwu", 35);
        Person maliu = new Person("maliu", 25);
        personList.add(zhangsan);
        personList.add(lisi);
        personList.add(wangwu);
        personList.add(maliu);

        int asInt = personList.stream().mapToInt(Person::getAge).max().getAsInt();
        System.out.println(asInt);

        List<Integer> collect = personList.stream().map(Person::getAge).collect(Collectors.toList());
        int size = collect.size();
        System.out.println(size);
        Integer max = Collections.max(collect);
        System.out.println(max);

        int age = personList.stream().max(Comparator.comparingInt(Person::getAge)).get().getAge();
        System.out.println(age);

        LinkedHashMap<Object, Object> map = new LinkedHashMap<>();

        List<Integer> integers = Arrays.asList(1, 3, 6, 9, 0);

        System.out.println(Stream.of("ab", "cd", "efg").map(str -> str.substring(0, 1)
                .toUpperCase() + str.substring(1).toLowerCase()).reduce("", String::concat));

        boolean b = Stream.of("ab", "cd", "efg").anyMatch(str -> str.equals("ab"));
        System.out.println(b);

        System.out.println("--------------------------------------------------------------");
        INTEGER_LIST.forEach(System.out::println);

        testReduce(INTEGER_LIST);
        testGroupBy();
    }

    private static void testReduce(List<Integer> list) {
        int sum = 0;
        for (Integer i : list) {
            sum += i;
        }
        System.out.println(sum);

        //  等价于上面的代码。执行逻辑如下：
        // a 相当于 sum，初始值为0，b相当于i，第一次是0+i，得到结果赋值给第二次遍历的a，再执行a + b
        // 以下代码就是list对象中的数字累加
        int i = list.stream().reduce(0, Integer::sum);
//        int i = list.stream().reduce(0, (a, b) -> a + b);
        System.out.println(i);

        // 同理。第一次遍历，a=1,然后a*集合中第一次遍历的数字，这里假设为i，即1 * i
        // 第二轮，a= 1* i，然后a* 第二轮的i
        int j = list.stream().reduce(1, (a, b) -> a * b);
        System.out.println(j);
    }

    private static void testGroupBy() {
        List<String> list = Arrays.asList("apple", "banana", "orange");
        Map<Integer, List<String>> map = new HashMap<>();
        for (String s : list) {
            int length  = s.length();
            if (!map.containsKey(length)) {
                map.put(length, new ArrayList<>());
            }
            map.get(length).add(s);
        }
        System.out.println(map);

        Map<Integer, List<String>> group = list.stream().collect(Collectors.groupingBy(String::length));
        System.out.println(group);
    }
}
