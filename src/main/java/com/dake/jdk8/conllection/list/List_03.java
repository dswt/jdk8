package com.dake.jdk8.conllection.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class List_03 {

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        List<Integer> list = new ArrayList<>();
//        list.add(1);
//        Integer integer = Optional.ofNullable(list).map(l -> l.get(0)).orElseGet(() -> 1);
//        System.out.println(integer);
        boolean present = Optional.of(list).isPresent();
        System.out.println(present);
    }
}
