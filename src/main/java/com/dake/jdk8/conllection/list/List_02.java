package com.dake.jdk8.conllection.list;

import com.dake.jdk8.vo.reflect.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class List_02 {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("a[0]123", "a[0]123", "a[1]123", "a[1]123", "a[2]123", "a[2]123");
        Map<String, List<String>> collect = list.stream().collect(Collectors.groupingBy(a ->
                a.substring(0, a.indexOf("]") + 1), Collectors.toList()));
        collect.forEach((k, v) -> System.out.println("key = " + k + ", v = " + v));

        testArrayListCaptal();
        System.out.println("-----------------------------");
        test();
    }

    /**
     * @return
     */
    public static void testArrayListCaptal() {
        List list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>(3);
        list2.add(2);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        System.out.println(list2.get(0));

        Person person = new Person();
    }

    public static void test() {
        List<String> stringList = new ArrayList<>();
        for (String s : stringList) {
            System.out.println("空对象进入了循环");
        }

        stringList.forEach(s -> System.out.println("forecach循环空集合"));
    }

}
