package com.dake.jdk8.util;

import javax.annotation.PostConstruct;

public class SpringBeanInitialization{

    private static SpringBeanInitialization beanInitialization;
    private static ReflectionUtils reflectionUtils;

    /**
     * 通过{@code @PostConstruct}修饰的方法会在服务器加载{@code servlet}的时候运行，并且只会被服务器执行一次。
     * {@code @PostConstruct}在构造函数之后执行，init方法之前执行；
     * {@code javax.annotation.PreDestroy}方法在{@code destory()}方法之后执行。
     * {@link #init()}先给该类赋值，然后通过注入方式赋值。
     */
    @PostConstruct
    public void init() {
        beanInitialization = this;
        SpringBeanInitialization.reflectionUtils = this.reflectionUtils;
    }
}
