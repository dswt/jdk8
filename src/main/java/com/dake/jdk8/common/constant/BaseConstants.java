package com.dake.jdk8.common.constant;

public class BaseConstants {
    private BaseConstants() {
        throw new IllegalStateException("BaseConstants class");
    }

    /** 中文逗号 */
    public static final String CHINESE_COMMA = "，";
    /** 英文逗号 */
    public static final String ENGLISH_COMMA = ",";
    /** 点 */
    public static final String DOT = ".";
    /** 下划线 */
    public static final String UNDERLINE = "_";
    /** 中文冒号 */
    public static final String CHINESE_COLON = "：";
    /** 英文冒号 */
    public static final String ENGLISH_COLON = ":";
    /** 井号 */
    public static final String POUND_SIGN = "#";
    /** 中文左方括弧 */
    public static final String CHINESE_LEFT_SQUARE_BRACKET = "【";
    /** 英文右方括弧 */
    public static final String CHINESE_RIGHT_SQUARE_BRACKET = "】";
    /** 英文左方括弧 */
    public static final String ENGLISH_LEFT_SQUARE_BRACKET = "[";
    /** 英文右方括弧 */
    public static final String ENGLISH_RIGHT_SQUARE_BRACKET = "]";

}
