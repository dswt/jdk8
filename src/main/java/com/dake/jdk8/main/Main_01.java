package com.dake.jdk8.main;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Main_01 {
    private static Random random;

    {
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        boolean flag = true;
        Boolean flag1 = true;
        Boolean flag2 = new Boolean(true);
        String flag3 = "a";
        boolean[] flag4 = {true};


        testFlag(flag, flag1, flag2,flag3, flag4[0], flag4);
        System.out.println(flag);
        System.out.println(flag1);
        System.out.println(flag2);
        System.out.println(flag3);
        System.out.println(flag4[0]);
    }

    private static void testFlag(boolean flag, Boolean flag1, Boolean flag2, String flag3, boolean flag40, boolean[] flag4) {
        flag = false;
        flag1 = false;
        flag1 = false;
        flag3 = "b";
        flag40 = false;
        flag4[0] = false;
    }

    private static int genarateRandom() {
        int i = random.nextInt(10);
        return i;
    }
}
